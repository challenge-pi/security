FROM debian:bullseye-slim

RUN apt-get update && \
    apt-get install -y \
    ca-certificates \
    git \
    wget \
    rpm

RUN wget https://github.com/aquasecurity/trivy/releases/download/v0.18.3/trivy_0.18.3_Linux-64bit.deb

RUN dpkg -i trivy_0.18.3_Linux-64bit.deb

ENTRYPOINT ["trivy"]
